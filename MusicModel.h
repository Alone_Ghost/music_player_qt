#ifndef MUSICMODEL_H
#define MUSICMODEL_H

#include <QAbstractListModel>

struct music
{
    QString name;
	QString fileName;
};

class MusicModel : public QAbstractListModel
{
public:
	MusicModel();
    enum roles
    {
        fileName = Qt::UserRole + 1
    };

public:
	int rowCount(const QModelIndex &parent) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	void addItem(QString fileName);
	void clear();

public:
	QVector<music> musics;
};

#endif // MUSICMODEL_H
