#include "mainpage.h"
#include "ui_mainpage.h"
#include <QListWidget>

MainPage::MainPage(QWidget *parent)
	: QDialog(parent)
	, ui(new Ui::MainPage)
{
	//	void doubleClicked(QListWidgetItem * listView);

	ui->setupUi(this);

	listView = new QListView(this);

	resume = new QPushButton("Resume");
	//    pause = new QPushButton("Pause");
	playList = new QMediaPlaylist();
	player = new QMediaPlayer();
	stop = new QPushButton("Stop");
	play = new QPushButton("Play");
	cancel = new QPushButton("Close");
	repeat = new QPushButton("Repeat");
	openFile = new QPushButton("Open File");
	slider = new QSlider(this);
	bar = new QProgressBar(this);
	hboxLayout = new QHBoxLayout;
	vboxLayout = new QVBoxLayout;
	hboxLayoutTwo = new QHBoxLayout;
	vboxlayoutForRepeat = new QVBoxLayout;

	//property
	slider->setOrientation(Qt::Horizontal);
	repeat->setCheckable(true);

	//icons
	play->setIcon(QIcon(":/img/Playe"));
	play->setIconSize(QSize(30, 30));
	play->setShortcut(tr("Ctrl+p"));
	stop->setIcon(QIcon(":/img/Stop"));
	stop->setIconSize(QSize(15, 15));
	stop->setShortcut(tr("ctrl+s"));
	openFile->setIcon(QIcon(":/img/Directory"));
	openFile->setIconSize(QSize(30, 30));
	openFile->setShortcut(tr("Ctrl+o"));
	repeat->setIcon(QIcon(":img/Repeat"));
	repeat->setIconSize(QSize(16, 16));
	cancel->setIcon(QIcon(":img/Exit"));
	cancel->setIconSize(QSize(18, 18));

	//color icon
	//hboxLayout->setStyleSheet
	play->setStyleSheet("background-color: rgb(102, 255, 102) ");
	stop->setStyleSheet("background-color: rgb(153, 153, 102) ");
	//for hbox and vbox
	vboxlayoutForRepeat->addWidget(play);
	vboxlayoutForRepeat->addWidget(repeat);
	hboxLayout->addWidget(stop);
	hboxLayout->addWidget(openFile);
	hboxLayout->addWidget(cancel);
	hboxLayoutTwo->addWidget(slider);
	hboxLayoutTwo->addWidget(bar);
	vboxLayout->addWidget(listView);
	vboxLayout->addLayout(vboxlayoutForRepeat);
	vboxLayout->addLayout(hboxLayout);
	vboxLayout->addLayout(hboxLayoutTwo);

	//for connect button
	connect(stop, SIGNAL(clicked()), SLOT(buttonStop()));
	connect(openFile, SIGNAL(clicked()), SLOT(openPageOpenFile()));
	connect(play, SIGNAL(clicked()), SLOT(buttonPlay()));
	connect(cancel, SIGNAL(clicked()), SLOT(buttonCancel()));
	connect(repeat, SIGNAL(clicked()), this, SLOT(buttonRepeat()));
	connect(listView, &QListView::doubleClicked, this, &MainPage::doubleClicked);

	connect(player, &QMediaPlayer::durationChanged, slider, &QSlider::setMaximum);
	connect(player, &QMediaPlayer::positionChanged, slider, &QSlider::setValue);
	connect(slider, &QSlider::sliderMoved, player, &QMediaPlayer::setPosition);
	connect(player, &QMediaPlayer::durationChanged, bar, &QProgressBar::setMaximum);
	connect(player, &QMediaPlayer::positionChanged, bar, &QProgressBar::setValue);

	this->setWindowTitle("Alone Player");
	this->setLayout(vboxLayout);
}

MainPage::~MainPage()
{
	delete ui;
}

void MainPage::buttonStop()
{
	player->stop();
	playList->removeMedia(0);
	play->setIcon(QIcon(":/img/Playe"));
	play->setIconSize(QSize(30, 30));
	play->setText("Play");
}

void MainPage::buttonPlay()
{

	if (play->text() == "Play") {
		player->setPlaylist(playList);
		player->play();
		play->setText("Pause");
		play->setIcon(QIcon(":img/Pause"));
		play->setIconSize(QSize(35, 35));

	} else if (play->text() == "Pause") {
		player->pause();
		play->setText("Resum");
		play->setIcon(QIcon(":/img/Playe"));
		play->setIconSize(QSize(30, 30));
		play->setShortcut(tr("Ctrl+p"));

	} else if (play->text() == "Resum") {
		player->play();
		play->setIcon(QIcon(":img/Pause"));
		play->setText("Pause");
		play->setIconSize(QSize(35, 35));
	}
}

void MainPage::openPageOpenFile()
{
	musicModel.clear();
	QStringList openFileList = QFileDialog::getOpenFileNames(this, tr("Open Music"), "/home",
															 tr("Music Files (*.mp3 )"));
	for (const QString &openFile : openFileList) {
		playList->addMedia(QUrl::fromLocalFile(openFile));
		musicModel.addItem(QFileInfo(openFile).filePath());
	}
	listView->setModel(&musicModel);
}

void MainPage::buttonCancel()
{
	this->close();
}

void MainPage::buttonRepeat()
{
	if (repeat->isChecked()) {
		playList->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
		return;
	} else if (repeat->isChecked() == false)
		playList->disconnect();
}

void setCurrentItem(QListWidgetItem *item);

void MainPage::doubleClicked(const QModelIndex &index)
{
	player->stop();
    QString curretnItemSelected = musicModel.data(index, MusicModel::roles::fileName).toString();
		//		= item->listWidget()->currentItem()->data(MusicModel::roles::fileName).toString();


	if (play->text() == "Play") {
		player->setMedia(QUrl::fromLocalFile(curretnItemSelected));
		player->play();
		play->setText("Pause");
		play->setIcon(QIcon(":img/Pause"));
		play->setIconSize(QSize(35, 35));

	} else if (play->text() == "Pause") {
		player->pause();
		play->setText("Resum");
		play->setIcon(QIcon(":/img/Playe"));
		play->setIconSize(QSize(30, 30));
		play->setShortcut(tr("Ctrl+p"));

	} else if (play->text() == "Resum") {
		player->play();
		play->setIcon(QIcon(":img/Pause"));
		play->setText("Pause");
		play->setIconSize(QSize(35, 35));
	}
}
