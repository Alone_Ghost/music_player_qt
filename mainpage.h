#ifndef MAINPAGE_H
#define MAINPAGE_H

#include <QDialog>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QListWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QMessageBox>
#include <QProgressBar>
#include <QPushButton>
#include <QSlider>
#include <QStatusBar>
#include <QString>
#include <QToolButton>
#include <QVBoxLayout>

#include "MusicModel.h"

namespace Ui {
class MainPage;
}

class MainPage : public QDialog
{
	Q_OBJECT

public:
	explicit MainPage(QWidget *parent = nullptr);
	~MainPage();

private:
	Ui::MainPage *ui;
	QPushButton *stop;
	QPushButton *play;
	QPushButton *cancel;
	QPushButton *openFile;
	QPushButton *repeat;
	QHBoxLayout *hboxLayout;
	QHBoxLayout *hboxLayoutTwo;
	QVBoxLayout *vboxlayoutForRepeat;
	QVBoxLayout *vboxLayout;
	QToolButton *buttonrepat;
	QPushButton *resume;
	QPushButton *pause;
	QMediaPlayer *player;
	QMediaPlaylist *playList;
	QSlider *slider;

	QListView *listView;

	QProgressBar *bar;

	//model
	MusicModel musicModel;

private slots:
	void buttonStop();
	void buttonPlay();
	void openPageOpenFile();
	void buttonCancel();
	void buttonRepeat();

	void doubleClicked(const QModelIndex &index);
};

#endif // MAINPAGE_H
