#include "MusicModel.h"

#include <QFileInfo>

MusicModel::MusicModel()
{}

int MusicModel::rowCount(const QModelIndex & /*parent*/) const
{
	return musics.size();
}

QVariant MusicModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (index.row() > musics.size() || index.row() < 0) {
		return QVariant();
	}

	if (role == Qt::DisplayRole) {
		return musics[index.row()].name;
	} else if (role == fileName) {

		return musics[index.row()].fileName;
	}
	return QVariant();
}

void MusicModel::addItem(QString fileName)
{
	music m;
	QFileInfo fi(fileName);
	m.name = fi.fileName();
	m.fileName = fi.filePath();
	musics.append(m);
}

void MusicModel::clear()
{
	musics.clear();
}
